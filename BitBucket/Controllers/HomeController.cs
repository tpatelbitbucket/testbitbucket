﻿using BitBucket.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BitBucket.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult Integration()
        {
            return View();
        }

        /// <summary>
        /// Git Lab Json.
        /// </summary>
        /// <param name="gitLabJson"></param>
        /// <returns></returns>
        public ActionResult GitLabJson(string gitLabJson)
        {
            try
            {
                //JObject jObjGitLabJson = JObject.Parse(gitLabJson);
                //GitLab obj = JsonConvert.DeserializeObject<GitLab>(gitLabJson);
            }
            catch (Exception ex)
            { }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Git hub json
        /// </summary>
        /// <param name="gitHubJson"></param>
        /// <returns></returns>
        public ActionResult GitHubJson(string gitHubJson)
        {
            try
            {
                //GitHub obj = JsonConvert.DeserializeObject<GitHub>(gitHubJson);
            }
            catch (Exception ex)
            { }  
            return Json("Success",JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Bit bucket json.
        /// </summary>
        /// <param name="bitBucketJson"></param>
        /// <returns></returns>
        public ActionResult BitBucketJson(string bitBucketJson)
        {
            try
            {
                BitBucketIntegration objBitBucket = JsonConvert.DeserializeObject<BitBucketIntegration>(bitBucketJson);
            }
            catch (Exception ex)
            { }
            return Json("Success",JsonRequestBehavior.AllowGet);
        }
    }
}